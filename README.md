# Estrellas PES API

## Setting the Development Evironment

- Clone the repo
```
git clone https://gitlab.com/mocitech/echampionship_api.git
```

- Install dependencies
```
cd echampionship_api
```
```
composer install
```

- Copy and replace `default.settings.php` to `settings.php`
```
cd web/sites/default
```
```
cp default.settings.php settings.php
```

- Copy this in the last part of the `settings.php` file and set your database credentials
```php
$databases['default']['default'] = array (
  'database' => '<DATABASE_NAME>',
  'username' => '<DATABASE_USER>',
  'password' => '<DATABASE_PASSWORD>',
  'prefix' => '',
  'host' => '127.0.0.1',   // OR locahost
  'port' => '<DATABASE_PORT>',
  // Change this if you are using other database 👇️👇️
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
```

- Generate a new `hash_salt`, run
````
drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55) . "\n";'
````
- Add the generated hash to the `settings.php` file.
```php
$settings['hash_salt'] = '<GENERATED_HASH_SALT>';
```
- Add the folder for saving the configurations to the `settings.php` file.
```php
$settings['config_sync_directory'] = 'sites/config/sync';
```

- Open your localhost 👍️

# Development Workflow
### In the local machine
- Make changes `master` branch
- `prod` branch is blocked by default.
- If you did changes in the menus. Export them
```
drush em
```
- If some changes were made in the CMS, export the configuration
```
drush cex
```
- Make a commit
- Push your changes to `master` branch
```
git push origin master
```
### In GitLab
- Create a [Merge Request](https://gitlab.com/mocitech/echampionship_api/-/merge_requests) from `master` to `prod`
- Approve the `Merge Request`

### In the Lighsail instance
- Make sure you are in `prod` branch
```
git branch
```
- Pull the changes
- Import the configuration
```
drush cim
```
- If you did changes in the menus. Import them as well
```
drush im
```
- In order to override exisiting menus, use `Full`
```
What import style would you like?:
  [0] Cancel
  [1] Full
  [2] Safe
  [3] Force
```

# Misc

## Get your Lightsail `application password`
In the Lightsail instance, run
````
cat bitnami_application_password
````
*This password is used for:*
- MySQL `root` user
- PhpMyAdmin user

## Connect to the instance from your local machine using SSH
- Download the private key from here. https://lightsail.aws.amazon.com/ls/webapp/account/keys
- Change permission to the downloaded key
````
chmod 600 LightsailDefaultKey-us-east-1.pem
````
- Connect to the instance
````
ssh -i LightsailDefaultKey-us-east-1.pem bitnami@3.92.87.87
````

## Connect to PhpMyAdmin
See first. 👆️ `Connect to the instance from your local machine using SSH` 👆️
- Run:
```
ssh -N -L 8888:127.0.0.1:80 -i LightsailDefaultKey-us-east-1.pem bitnami@3.92.87.87
```
- Then, you now can go to: http://127.0.0.1:8888/phpmyadmin
- Use this credentials:
  |Username| root |
  |Password| |

## Create a database backup from the Lightsail instance
````
mysqldump -u [username] -p [databaseName] > estrellas_pes__prod-$(date +%F).sql
````
For example:
````
mysqldump -u mocion-dev -p estrellas_pes__prod > estrellas_pes__prod-$(date +%F).sql
````

## Download a database backup from the Lightsail instance
- From your local machine, run
````
scp -i LightsailDefaultKey-us-east-1.pem bitnami@3.92.87.87:<REMOTE_LOCATION> <LOCAL_DESTINATION>
````
example:
````
scp -i LightsailDefaultKey-us-east-1.pem bitnami@3.92.87.87:~/htdocs/estrellas-pes/estrellas_pes__prod-2020-04-19.sql .
````

## Restore a database backup
- If you are using MAMP:
````
/Applications/MAMP/Library/bin/mysql --host=localhost -uroot -p echampionship_api < <DATABASE_BACKUP_LOCATION>
````

If you get this error

`ERROR 1273 (HY000) at line 25: Unknown collation: 'utf8mb4_0900_ai_ci'`

Please run
```
sed -i '' 's/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/g' <DATABASE_BACKUP>
```
For example:
```
sed -i '' 's/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/g' estrellas_pes__prod-2020-04-25.sql
```

- If you are **NOT** using MAMP:
````
mysql -u root -p echampionship_api < <DATABASE_BACKUP_LOCATION>
````




