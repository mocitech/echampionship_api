<?php

/**
 * @file
 * Contains \Drupal\estrellas_pes\Controller\EstrellasPesController.
 */

namespace Drupal\estrellas_pes\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller routines for estrellas_pes routes.
 */
class EstrellasPesController extends ControllerBase
{

  /**
   * Callback for `api/get-site-config` API method.
   */
  public function getSiteConfig(Request $request)
  {
    $config = \Drupal::config('system.site');

    $response['data'] = $config->get();
    $response['method'] = 'GET';

    return new JsonResponse($response);
  }
}
