<?php

namespace Drupal\estrellas_pes\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\SiteInformationForm;
use GuzzleHttp\Exception\RequestException;

class ExtendedSiteInformationForm extends SiteInformationForm
{

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $site_config = $this->config('system.site');
    $form =  parent::buildForm($form, $form_state);

    $form['site_information']['site_description'] = [
      '#type' => 'textfield',
      '#title' => t('Site Description'),
      '#default_value' => $site_config->get('site_description') ?: "",
      '#description' => t("Site description that will appear in Google"),
    ];

    $form['site_information']['is_site_active'] = [
      '#type' => 'checkbox',
      '#title' => t('Activate the site'),
      '#default_value' => $site_config->get('is_site_active') ? TRUE : FALSE,
      '#description' => t("If this is not checked. The Landing page is going to be shown"),
    ];

    $form['site_information']['spotify_playlist_url'] = [
      '#type' => 'textfield',
      '#title' => t('Spotify playlist URL'),
      '#default_value' => $site_config->get('spotify_playlist_url') ?: ""
    ];

    $form['site_information']['is_spotify_widget_compact'] = [
      '#type' => 'checkbox',
      '#title' => t('Spotify Widget Compacted'),
      '#default_value' => $site_config->get('is_spotify_widget_compact') ? TRUE : FALSE,
      '#description' => t("
        <br>
        <b>Default</b>
        <br>
        <img width='10%' src='https://developer.spotify.com/assets/new-spotify-play-button-grey.png'>
        <br>
      "),

    ];

    $form['site_information']['social_networks'] = array(
      '#type' => 'fieldset',
      '#title' => t('Social Networks'),
    );

    $form['site_information']['social_networks']['twitch'] = [
      '#type' => 'textfield',
      '#title' => t('Twitch URL'),
      '#default_value' => $site_config->get('social_networks')['twitch'] ?: "",
    ];

    $form['site_information']['social_networks']['facebook'] = [
      '#type' => 'textfield',
      '#title' => t('Facebook URL'),
      '#default_value' => $site_config->get('social_networks')['facebook'] ?: "",
    ];

    $form['site_information']['social_networks']['youtube'] = [
      '#type' => 'textfield',
      '#title' => t('Youtube URL'),
      '#default_value' => $site_config->get('social_networks')['youtube'] ?: "",
    ];

    $form['site_information']['social_networks']['instagram'] = [
      '#type' => 'textfield',
      '#title' => t('Instagram URL'),
      '#default_value' => $site_config->get('social_networks')['instagram'] ?: "",
    ];

    $form['site_information']['social_networks']['twitter'] = [
      '#type' => 'textfield',
      '#title' => t('Twitter URL'),
      '#default_value' => $site_config->get('social_networks')['twitter'] ?: "",
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('system.site')
      ->set('site_description', $form_state->getValue('site_description'))
      ->save();

    $this->config('system.site')
      ->set('is_site_active', $form_state->getValue('is_site_active'))
      ->save();

    $this->config('system.site')
      ->set('spotify_playlist_url', $form_state->getValue('spotify_playlist_url'))
      ->save();

    $this->config('system.site')
      ->set('is_spotify_widget_compact', $form_state->getValue('is_spotify_widget_compact'))
      ->save();

    $this->config('system.site')
      ->set('social_networks', array(
        "twitch" => $form_state->getValue('twitch'),
        'twitter' => $form_state->getValue('twitter'),
        'facebook' => $form_state->getValue('facebook'),
        'youtube' => $form_state->getValue('youtube'),
        'instagram' => $form_state->getValue('instagram')
      ))
      ->save();

    try {
      $client = \Drupal::httpClient();

      $endpoint = 'https://api.netlify.com/build_hooks/5e9e3708079db797ddf22f3c';

      $request = $client->request('POST', $endpoint);
    } catch (RequestException $e) {
    }

    /**
     * HOW TO REMOVE A CONFIGURATION PROGRAMATICALLY
     *     \Drupal::configFactory()->getEditable('system.site')->clear('instagram');
     */

    parent::submitForm($form, $form_state);
  }
}
